from fabric.api import local, hosts, run, cd, prefix, sudo, settings

def prep_deploy(branch_name):
    local('pip freeze > requirements.txt')
    local('./manage.py test sealab')
    local('./manage.py test website')
    migrate_local()
    with settings(warn_only=True):
        local('git add -A . && git commit')
    local('git push origin %s' % branch_name)

@hosts('eli@eeman')
def deploy_prod():
    app_dir = '/webapps/sealab.com'
    git_dir = '/webapps/sealab.com/sealab.com'
    with cd(git_dir), prefix('export DJANGO_SETTINGS_MODULE=sealab.settings.production'):
        run('git pull origin master')
        run('pip install -r requirements.txt --allow-all-external')
        run('./manage.py migrate')
        run('./manage.py collectstatic')
        run('./manage.py clear_cache')
        sudo('service nginx restart sealab')
    with cd(app_dir), prefix('export DJANGO_SETTINGS_MODULE=sealab.settings.production'):
        run('kill -HUP `cat tmp/gunicorn.pid`')

@hosts('eli@eeman')
def update_prod():
    app_dir = '/webapps/sealab.com'
    git_dir = '/webapps/sealab.com/sealab.com'
    with cd(git_dir):
        run('git pull origin master')
    with cd(app_dir):
        sudo('apt-get update')
        sudo('apt-get upgrade')
        with prefix('pyenv activate sealab'):
            run('pip install -r requirements.txt --allow-all-external')

def migrate_local():
    #website app
    with settings(warn_only=True):
        local('..manage.py makemigrations')
    local('./manage.py migrate')
