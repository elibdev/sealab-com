from django.shortcuts import render

def home(request):
    return render(request, 'website/home-ajax.html')

def sounds(request):
    return render(request, 'website/sounds-ajax.html')

def pictures(request):
    return render(request, 'website/pictures-ajax.html')

def shows(request):
    return render(request, 'website/shows-ajax.html')

def contact(request):
    return render(request, 'website/contact-ajax.html')

