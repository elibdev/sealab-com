from django.conf.urls import patterns, url

from website import ajax

urlpatterns = patterns('',
    url(r'^$', ajax.home),
    url(r'^sounds/$', ajax.sounds),
    url(r'^pictures/$', ajax.pictures),
    url(r'^shows/$', ajax.shows),
    url(r'^contact/$', ajax.contact),
)
