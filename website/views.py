from django.shortcuts import render

def home(request):
    return render(request, 'website/home.html')

def sounds(request):
    return render(request, 'website/sounds.html')

def pictures(request):
    return render(request, 'website/pictures.html')

def shows(request):
    return render(request, 'website/shows.html')

def contact(request):
    return render(request, 'website/contact.html')

