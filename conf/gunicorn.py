pidfile = '/webapps/sealab.com/tmp/gunicorn.pid'
bind = 'sealabmusic.com'
workers = 3
accesslog = '/webapps/sealab.com/logs/gunicorn-access.log'
errorlog = '/webapps/sealab.com/logs/gunicorn-error.log'

