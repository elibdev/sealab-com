function updateColors() {
    var relativeURL = window.location.pathname;
    if (relativeURL == '/pictures') {
        window.morphInProgress = false;
        return;
    }
    window.morphInProgress = true;
    var n = 4;
    var back = $('.bg-base');
    index = window.morphInfo.index % n;
    window.morphInfo = {
        index: index,
        nextIndex: ((index +1) % n),
    }
    var nextIndex = window.morphInfo.nextIndex;
    var front = $("<div class='bg-base bg-color-" + nextIndex + "'></div>");

    var topMostBack = back;
    if(back.length > 1) {
        topMostBack = $(back[back.length - 1]);
    }
    topMostBack.after(front);
    front.css('opacity', 0);

    //fade front over back, then remove back
    front.velocity({
        opacity: 1.0
    }, 5000, "linear", function() {
        back.remove();
        updateColors();
    });

    window.morphInfo.index++;
}

window.onpopstate = function(event) {
    handlePop(event.state);
}

function fadeBlack(state) {
    if (state == '/pictures') {
        var back = $('.bg-base');
        if(back.length > 1) {
            back = $(back[back.length - 1]);
        }
        var black = $("<div class='bg-base bg-color-black'></div>");
        black.css('opacity', 0);
        back.after(black);
        black.velocity({
            opacity: 1.0
        }, 1000, "linear");
    }
    else {
        var black = $('.bg-color-black');
        if (black) {
            black.velocity({
                opacity: 0
            }, 1000, "linear", function() {
                black.remove();
            });
        }
    }
}

function handlePop(state) {
    $.ajax({
        url:'/ajax' + state,
        success: function(html) {
            c = $('.content');
            c.after(html);
            c.remove();
            fadeBlack(state);
            updateTitle();
            if(!window.morphInProgress) {
                updateColors();
            }
        },
        error: function(xhr) {
            if (xhr.status==408 && retries < 3) {
                $.ajax(this);
            }
        }
    })
}

function updateTitle() {
    var relativeURL = window.location.pathname;
    var title;
    if (relativeURL == '/') {
        title = 'News'
    }
    else {
        title = relativeURL.charAt(1).toUpperCase() + relativeURL.slice(2);
    }
    document.title = 'Sealab | ' + title;
}

$(document).ready(function() {

    updateTitle();

    var bg = $(".bg-base");

    $('a[rel*="morph"]').click(function () {
        var link = this.getAttribute('href');
        if (link == window.location.pathname) {
            return false;
        }
        var retries = 0;
        window.history.pushState(link, '', link);
        handlePop(link);
        return false;
    });

    //need this so that refresh doesn't break things
    var relativeURL = window.location.pathname;
    window.history.replaceState(relativeURL, '', relativeURL);

    window.morphInfo = {
        index: 0,
    }
    updateColors();
});
