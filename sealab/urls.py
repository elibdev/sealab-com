from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
    url(r'^$', 'website.views.home', name='home'),
    url(r'^sounds$', 'website.views.sounds', name='sounds'),
    url(r'^pictures$', 'website.views.pictures', name='pictures'),
    url(r'^shows$', 'website.views.shows', name='shows'),
    url(r'^contact$', 'website.views.contact', name='contact'),
    url(r'^ajax/', include('website.ajax-urls', namespace='ajax')),
    url(r'^admin/', include(admin.site.urls)),
)
